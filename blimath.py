# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

"""
Simple functional math library (keep all args immutable!).

Based on Blender's BLI_math
"""

import math

# -----------------------------------------------------------------------------
# BLI_vector

def add_v2v2(v0, v1):
    return (
        v0[0] + v1[0],
        v0[1] + v1[1],
        )


def add_v3v3(v0, v1):
    return (
        v0[0] + v1[0],
        v0[1] + v1[1],
        v0[2] + v1[2],
        )


def add_vnvn(v0, v1):
    return tuple(a + b for a, b in zip(v0, v1))


def sub_v2v2(v0, v1):
    return (
        v0[0] - v1[0],
        v0[1] - v1[1],
        )


def sub_v3v3(v0, v1):
    return (
        v0[0] - v1[0],
        v0[1] - v1[1],
        v0[2] - v1[2],
        )


def sub_vnvn(v0, v1):
    return tuple(a - b for a, b in zip(v0, v1))


def negated_vn(v0):
    return tuple(-a for a in v0)


def mul_v2_fl(v0, f):
    return (
        v0[0] * f,
        v0[1] * f,
        )


def mul_v3_fl(v0, f):
    return (
        v0[0] * f,
        v0[1] * f,
        v0[2] * f,
        )


def mul_vn_fl(v0, f):
    return tuple(a * f for a in v0)


def dot_v2v2(v0, v1):
    return (
        (v0[0] * v1[0]) +
        (v0[1] * v1[1])
        )


def dot_v3v3(v0, v1):
    return (
        (v0[0] * v1[0]) +
        (v0[1] * v1[1]) +
        (v0[2] * v1[2])
        )


def dot_vnvn(v0, v1):
    return sum(a * b for a, b in zip(v0, v1))


def len_squared_v2v2(v0, v1):
    d = sub_v2v2(v0, v1)
    return dot_v2v2(d, d)


def len_squared_v3(v0):
    return dot_v3v3(v0, v0)

def len_squared_vn(v0):
    return dot_vnvn(v0, v0)

def len_squared_v3v3(v0, v1):
    d = sub_v3v3(v0, v1)
    return len_squared_v3(d)

def len_squared_vnvn(v0, v1):
    d = sub_vnvn(v0, v1)
    return len_squared_vn(d)


def len_v2v2(v0, v1):
    return math.sqrt(len_squared_v2v2(v0, v1))


def len_v3v3(v0, v1):
    return math.sqrt(len_squared_v3v3(v0, v1))


def len_vnvn(v0, v1):
    return math.sqrt(len_squared_vnvn(v0, v1))


def len_vn(v0):
    return math.sqrt(len_squared_vn(v0))


def normalized_vn(v0):
    d = len_squared_vn(v0)
    if d != 0:
        d = math.sqrt(d)
        return tuple(a / d for a in v0)
    else:
        return tuple(0.0 for a in v0)


def line_point_side_of(l1, l2, pt):
    return (((l1[0] - pt[0]) * (l2[1] - pt[1])) -
            ((l2[0] - pt[0]) * (l1[1] - pt[1])))


def cross_v2v2(v0, v1):
    return (v0[0] * v1[1] -
            v0[1] * v1[0])


def cross_v3v3(v0, v1):
    return (
        (v0[1] * v1[2]) - (v0[2] * v1[1]),
        (v0[2] * v1[0]) - (v0[0] * v1[2]),
        (v0[0] * v1[1]) - (v0[1] * v1[0]),
        )


# ------------------------------------------------------------------------------
# BLI_math_geom


def plane_point_side_v3(p, v):
    return dot_v3v3(p, v) + p[3]


def line_point_factor_v2(p, l1, l2, default=0.0):
    u = sub_v2v2(l2, l1)
    h = sub_v2v2(p, l1)
    dot = dot_v2v2(u, u)
    return (dot_v2v2(u, h) / dot) if dot != 0.0 else default


def line_point_factor_v2(p, l1, l2, default=0.0):
    u = sub_v2v2(l2, l1)
    h = sub_v2v2(p, l1)
    dot = dot_v2v2(u, u)
    return (dot_v2v2(u, h) / dot) if dot != 0.0 else default


# intersection function
def isect_line_plane_v3(p0, p1, p_co, p_no, epsilon=1e-6):
    """
    p0, p1: define the line
    p_co, p_no: define the plane:
        p_co is a point on the plane (plane coordinate).
        p_no is a normal vector defining the plane direction; does not need to be normalized.

    return a Vector or None (when the intersection can't be found).
    """

    u = sub_v3v3(p1, p0)
    dot = dot_v3v3(p_no, u)

    if abs(dot) > epsilon:
        # the factor of the point between p0 -> p1 (0 - 1)
        # if 'fac' is between (0 - 1) the point intersects with the segment.
        # otherwise:
        #  < 0.0: behind p0.
        #  > 1.0: infront of p1.
        w = sub_v3v3(p0, p_co)
        fac = -dot_v3v3(p_no, w) / dot
        u = mul_v3_fl(u, fac)
        return add_v3v3(p0, u)
    else:
        # The segment is parallel to plane
        return None


def isect_seg_seg_v2_point_nice(v0, v1, v2, v3):

    v10 = sub_v2v2(v1, v0)
    v32 = sub_v2v2(v3, v2)

    div = cross_v2v2(v10, v32)

    if div == 0.0:
        return None

    u = cross_v2v2(v1, v0)
    v = cross_v2v2(v3, v2)

    vi = (
        ((v32[0] * u) - (v10[0] * v)) / div,
        ((v32[1] * u) - (v10[1] * v)) / div,
        )

    if 0:
        fac = line_point_factor_v2(vi, v0, v1, default=-1.0)
        if fac < 0.0 or fac > 1.0:
            return None

        fac = line_point_factor_v2(vi, v2, v3, default=-1.0)
        if fac < 0.0 or fac > 1.0:
            return None
    else:
        # no need to check for divide by zero, div would be zero above
        # line_point_factor_v2
        h = sub_v2v2(vi, v0)
        dot = dot_v2v2(v10, v10)
        fac = (dot_v2v2(v10, h) / dot)
        if fac < 0.0 or fac > 1.0:
            return None

        # line_point_factor_v2
        h = sub_v2v2(vi, v2)
        dot = dot_v2v2(v32, v32)
        fac = (dot_v2v2(v32, h) / dot)
        if fac < 0.0 or fac > 1.0:
            return None


    return vi


def closest_to_line_v3(p, l1, l2):
    u = sub_v3v3(l2, l1)
    h = sub_v3v3(p, l1)
    l = dot_v3v3(u, h) / dot_v3v3(u, u)
    cp = (l1[0] + u[0] * l,
          l1[1] + u[1] * l,
          l1[2] + u[2] * l,
          )
    return cp, l


def closest_to_line_v2(p, l1, l2):
    u = sub_v2v2(l2, l1)
    h = sub_v2v2(p, l1)
    l = dot_v2v2(u, h) / dot_v2v2(u, u)
    cp = (l1[0] + u[0] * l,
          l1[1] + u[1] * l,
          )
    return cp, l


def closest_to_line_segment_v2(p, l1, l2):
    cp, fac = closest_to_line_v2(p, l1, l2)
    # flip checks for !finite case (when segment is a point)
    if not (fac > 0.0f):
        return l1
    elif not (fac < 1.0f):
        return l2
    else:
        return cp


def closest_to_line_segment_v3(p, l1, l2):
    cp, fac = closest_to_line_v3(p, l1, l2)
    # flip checks for !finite case (when segment is a point)
    if not (fac > 0.0f):
        return l1
    elif not (fac < 1.0f):
        return l2
    else:
        return cp


def dist_squared_to_line_segment_v2(p, l1, l2):
    closest = closest_to_line_segment_v2(closest, p, l1, l2);
    return len_squared_v2v2(closest, p)


def dist_squared_to_line_segment_v3(p, l1, l2):
    closest = closest_to_line_segment_v3(closest, p, l1, l2);
    return len_squared_v3v3(closest, p)


def point_in_slice_v3(p, v1, l1, l2):
    cp = closest_to_line_v3(v1, l1, l2)[0]
    q = sub_v3v3(cp, v1)
    rp = sub_v3v3(p, v1)
    h = dot_v3v3(q, rp) / dot_v3v3(q, q)
    return (h >= 0.0 and h <= 1.0)


def isect_point_tri_prism_v3(p, v1, v2, v3):
    return (point_in_slice_v3(p, v1, v2, v3) and
            point_in_slice_v3(p, v2, v3, v1) and
            point_in_slice_v3(p, v3, v1, v2))


def closest_to_plane3_v3(plane3, pt):
    len_sq = len_squared_v3(plane3)
    side = dot_v3v3(plane3, pt)

    # madd_v3_v3v3fl(r_close, pt, plane3, -side / len_sq)
    fac = -side / len_sq
    plane3_fac = mul_v3_fl(plane3, fac)
    return add_v3v3(pt, plane3_fac)


def dist_signed_squared_point_tri_plane_v3(p, v1, v2, v3):
    from math import copysign
    n = cross_v3v3(
            sub_v3v3(v3, v2),
            sub_v3v3(v1, v2))

    rp = sub_v3v3(p, v2)
    len_sq = dot_v3v3(n, n)
    side = dot_v3v3(rp, n)
    fac = side / len_sq
    return copysign(len_sq * (fac * fac), side)


def clip_segment_v3_plane_n(p1, p2, planes):
    dp = sub_v3v3(p2, p1)

    p1_fac = 0.0
    p2_fac = 1.0

    for p in planes:
        div = dot_v3v3(p[0])
        if div != 0.0:
            t = -plane_point_side_v3(p, p1)
            if div > 0.0:  # clip p1 lower bounds
                if t >= div:
                    return False, None, None
                if t > 0.0:
                    fac = (t / div)
                    if fac > p1_fac:
                        p1_fac = fac
                        if p1_fac > p2_fac:
                            return False, None, None
            elif div < 0.0:  # clip p2 upper bounds
                if t > 0.0:
                    return False, None, None
                if t > div:
                    fac = (t / div)
                    if fac < p2_fac:
                        p2_fac = fac
                        if p1_fac > p2_fac:
                            return False, None, None

    p1_clip = add_v3v3(p1, mul_v3_fl(dp, p1_fac))
    p2_clip = add_v3v3(p1, mul_v3_fl(dp, p2_fac))

    return True, p1_clip, p2_clip

if 0:
    from random import random

    def rand_vec():
        a = random()
        b = random()
        return a, b

    EPS = 0.001
    TOT = 100
    OK = 0

    for i in range(TOT):
        v0 = rand_vec()
        v1 = rand_vec()
        v2 = rand_vec()
        v3 = rand_vec()

        r0 = isect_seg_seg_v2_point(v0, v1, v2, v3)
        r1 = isect_seg_seg_v2_point_nice(v0, v1, v2, v3)

        if r0 is None and r1 is None:
            OK += 1
        elif r0 is None or r1 is None:
            # not ok
            pass
        elif len_squared_v2v2(r0, r1) < EPS:
            OK += 1
        else:
            # not ok
            pass

    print("Success:", OK, "of", TOT)

